import React from 'react';
// import { Font } from 'expo';
import * as Font from 'expo-font';
import { Splash, DrawerMenu } from './views';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;
        this.state = {hasLoaded: false};
    }

    componentWillMount() {
        Font.loadAsync({
            'Kayak Sans': require('./assets/fonts/kayak/400.otf'),
            'Kayak Sans Bold': require('./assets/fonts/kayak/700.otf'),
            'Roboto Light': require('./assets/fonts/roboto/300.ttf'),
            'Roboto': require('./assets/fonts/roboto/400.ttf'),
            'Roboto Medium': require('./assets/fonts/roboto/500.ttf'),
            'Roboto Bold': require('./assets/fonts/roboto/700.ttf')
        });
    }

    componentDidMount() {
        this.timeoutHandle = setTimeout(()=>{
            this.setState({hasLoaded: true});
        }, 3000);
    }

    render() {
        if(this.state.hasLoaded) {
            return (<DrawerMenu />);
        } else {
            return (<Splash />);
        }
    }
}
