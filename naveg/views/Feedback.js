import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';

export class Feedback extends React.Component {
    static navigationOptions = {
        title: 'Envie críticas/sugestões',
        drawerIcon: ({ tintColor }) => <Icon name="feedback" color={tintColor} />,
    };

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <View>
                <Text>
                    Envie críticas/sugestões
                </Text>
            </View>
        );
    }
}