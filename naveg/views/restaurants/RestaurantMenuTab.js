import React from 'react';
import { View, FlatList, TouchableWithoutFeedback } from 'react-native';
import NavegDish from '../../components/NavegDish';

export default class RestaurantMenuTab extends React.Component {
    render() {
        return (
        	<View style={{backgroundColor: '#fff', flex: 1}}>
	            <FlatList
	            	data={this.props.navigation.state.params.item.dishes}
	            	renderItem={({item}) =>
		            	<TouchableWithoutFeedback>
		            		<NavegDish dish={item} />
		            	</TouchableWithoutFeedback>
	            	}
	            	style={{paddingBottom: 40, width: '100%'}}
	            	keyExtractor={(item, index) => index.toString()}
	            />
            </View>
        );
    }
}