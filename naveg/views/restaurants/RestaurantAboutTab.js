import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import { Icon } from 'react-native-elements';
import NavegRestaurantAbout from '../../components/NavegRestaurantAbout';
import NavegUserCommentList from '../../components/NavegUserCommentList';
import NavegUserComment from '../../components/NavegUserComment';
import NavegRating from '../../components/NavegRating';

export default class RestaurantAboutTab extends React.Component {
    render() {
        return (
            <ScrollView style={{backgroundColor: '#fff'}}>
                <View>
                    <SwiperFlatList
                      autoplay
                      autoplayDelay={5}
                      autoplayLoop
                      index={0}
                      showPagination
                      paginationDefaultColor={'rgba(255,255,255,0.5)'}
                      paginationActiveColor={'rgba(255,255,255,1)'}
                      paginationStyleItem={styles.swiperPaginationDot}
                      style={styles.swiper}
                    >
                        <Image style={{width: 360, height: 152}} source={{uri: this.props.navigation.state.params.item.featuredImage}} />
                        <Image style={{width: 360, height: 152}} source={{uri: this.props.navigation.state.params.item.featuredImage}} />
                        <Image style={{width: 360, height: 152}} source={{uri: this.props.navigation.state.params.item.featuredImage}} />
                    </SwiperFlatList>
                </View>

                <View style={{padding: 15, paddingBottom: 30}}>
                    <Text style={{color: '#239B02', fontSize: 14, marginBottom: 5}}>{'Aberto'} </Text>
                    <Text style={{fontFamily: 'Kayak Sans Bold', color: '#515151', fontSize: 16, marginBottom: 15}}>{this.props.navigation.state.params.item.name}</Text>
                    <Text style={{color: '#9B9B9B', fontSize: 12}}>{this.props.navigation.state.params.item.description}</Text>
                </View>

                <View style={styles.infoBar}>
                    <Text style={{color: '#378E00', fontSize: 12, marginRight: 15}}>{this.props.navigation.state.params.item.averagePrice}</Text>
                    <Text style={{color: '#979797', fontSize: 12, marginLeft: 15, marginRight: 15}}>{this.props.navigation.state.params.item.category}</Text>
                    <Text style={{color: '#979797', fontSize: 12, marginLeft: 15, marginRight: 15}}>{this.props.navigation.state.params.item.distance}</Text>
                    <NavegRating value={this.props.navigation.state.params.item.rating} />
                </View>

                <NavegRestaurantAbout restaurant={this.props.navigation.state.params.item} />

                <NavegUserCommentList commentList={this.props.navigation.state.params.item.comments} />

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    swiper: {
        backgroundColor: '#eee',
    },
    swiperPaginationDot: {
        height: 6,
        marginLeft: 1,
        marginRight: 1,
        width: 6
    },
    infoBar: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#DEDEDE',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 8,
        paddingLeft: 60,
        paddingRight: 60,
        paddingTop: 8
    }
});
