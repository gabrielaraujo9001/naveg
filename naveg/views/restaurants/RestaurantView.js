import React from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import RestaurantAboutTab from './RestaurantAboutTab';
import RestaurantMenuTab from './RestaurantMenuTab';
import RestaurantPhotosTab from './RestaurantPhotosTab';

export const RestaurantView = createMaterialTopTabNavigator({
    Sobre: {
        screen: RestaurantAboutTab,
        navigationOptions: {
            title: 'Sobre'
        }
    },
    Cardapio: {
        screen: RestaurantMenuTab,
        navigationOptions: {
            title: 'Cardápio'
        }
    },
    Fotos: {
        screen: RestaurantPhotosTab,
        navigationOptions: {
            title: 'Fotos'
        }
    }
},{
    tabBarOptions: {
        activeTintColor: '#FE740E',
        inactiveTintColor: '#979797',
        style: {
            backgroundColor: '#fff'
        },
        indicatorStyle: {
            backgroundColor: '#FE740E',
            height: 4
        },
        labelStyle: {
            fontFamily: 'Roboto Medium',
            fontSize: 14
        }
    }
});
