import React from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableWithoutFeedback, ActivityIndicator, ScrollView} from 'react-native';
import { Button, Card, Icon } from 'react-native-elements';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Firebase from '../../config/Firebase';
import NavegRating from '../../components/NavegRating';
import NavegFavoriteButton from '../../components/NavegFavoriteButton';
import NavegHeader from '../../components/NavegHeader';

let ref = Firebase.database().ref();

export class RestaurantListView extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'NaVeg',
    header: props => <NavegHeader action="menu" {...props} />
  });

  constructor(props) {
    super(props);
    this.state = {loaded: false, restaurantes: null, melhores: null};
  }

  databaseLoaded = (snapshot) => {
    this.setState({
      loaded: true,
      restaurantes: snapshot.child('restaurants').val(),
      melhores: snapshot.child('bestRestaurants').val()
    });
  }

  componentWillMount() {
    ref.on('value', this.databaseLoaded);
  }

  render() {
    if(this.state.loaded) {
      return (
        <ScrollView>
            <View>
                <SwiperFlatList
                  autoplay
                  autoplayDelay={5}
                  autoplayLoop
                  index={0}
                  showPagination
                  paginationDefaultColor={'rgba(255,255,255,0.5)'}
                  paginationActiveColor={'rgba(255,255,255,1)'}
                  paginationStyleItem={styles.swiperPaginationDot}
                  style={styles.swiper}
                >
                    <Image style={{width: 360, height: 152}} source={{uri: 'http://sigmaapp.com.br/naveg/img/restaurants/restaurantes-para-veganos-en-roma.jpg'}} />
                    <Image style={{width: 360, height: 152}} source={{uri: 'http://sigmaapp.com.br/naveg/img/restaurants/gro-da-vida.jpg'}} />
                    <Image style={{width: 360, height: 152}} source={{uri: 'http://sigmaapp.com.br/naveg/img/restaurants/slider.jpg'}} />
                </SwiperFlatList>
            </View>

            <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
              <Text style={styles.listTitle}>MAIS BEM AVALIADOS</Text>
              <Text style={[styles.listTitle, {color: '#FA973C', fontSize: 12, fontFamily: 'Kayak Sans Bold', marginRight: 20}]}>Ver tudo</Text>              
            </View>

            <FlatList
              data={this.state.melhores}
              horizontal={true}
              renderItem={({item}) =>
                <TouchableWithoutFeedback id={item} onPress={() => this.props.navigation.navigate('RestaurantView', {item: this.state.restaurantes[item], title: this.state.restaurantes[item].name})}>
                  <View>
                    <View zIndex={1000} style={styles.bestRating}>
                      <NavegRating value={this.state.restaurantes[item].rating} />
                    </View>

                    <View zIndex={1} style={styles.bestCardContainer}>

                      <Card containerStyle={styles.bestCard}>

                        <Image source={{uri: this.state.restaurantes[item].bestLogo}} style={styles.bestRestaurantImage} />

                        <View style={{flexGrow: 1, flexBasis: `auto`, padding: 8}}>
                          <View style={{flex: 1, alignItems: 'flex-start', flexDirection: 'row', justifyContent: 'space-between'}}>
                              <Text numberOfLines={1} style={{fontFamily: 'Kayak Sans Bold', color: '#424242', fontSize: 14, marginBottom: 2}}>{this.state.restaurantes[item].name}</Text>
                              <View style={styles.favorite}>
                                <NavegFavoriteButton />
                              </View>
                          </View>

                          <View style={{flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start', marginBottom: 2}}>
                              <Text style={{color: '#9B9B9B', fontSize: 12, marginRight: 10}}>{this.state.restaurantes[item].category} </Text>
                              <Icon name="place" color="#9B9B9B" size={10}/>
                              <Text style={{color: '#9B9B9B', fontSize: 12}}>{this.state.restaurantes[item].distance}</Text>
                          </View>

                          <View style={{flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'}}>
                              <Text style={{color: '#239B02', fontSize: 12, marginRight: 10}}>{'Aberto'} </Text>
                              <Text style={{color: '#378E00', fontFamily: 'Roboto Bold', fontSize: 12}}>{this.state.restaurantes[item].averagePrice}</Text>
                          </View>
                        </View>

                      </Card>

                    </View>
                  </View>
                </TouchableWithoutFeedback>
              }
              style={{width: `auto`, marginLeft: 15, marginRight: 15}}
              keyExtractor={(item, index) => index.toString()}
            />

          <Text style={styles.listTitle}>RESTAURANTES PRÓXIMOS</Text>
          <FlatList
            data={this.state.restaurantes}
            renderItem={({item}) =>
            <TouchableWithoutFeedback id={item.id} onPress={() => this.props.navigation.navigate('RestaurantView', {item: item, title: item.name})}>
              <View style={styles.cardContainer}>
                <Card containerStyle={styles.card} flexDirection={'row'}>

                    <Image source={{uri: item.logo}} style={styles.restaurantImage} />

                    <View style={{flexGrow: 1, flexBasis: `auto`, padding: 8}}>
                        <View style={{flex: 1, alignItems: 'flex-start', flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text style={{fontFamily: 'Kayak Sans Bold', color: '#424242', fontSize: 16}}>{item.name}</Text>
                            <View style={styles.favorite}>
                              <NavegFavoriteButton />
                            </View>
                        </View>

                        <View style={{flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start'}}>
                            <Text style={{color: '#239B02', fontSize: 12, marginRight: 10}}>{'Aberto'} </Text>
                            <Text style={{color: '#9B9B9B', fontSize: 12, marginRight: 10}}>{item.category} </Text>
                            <Icon name="place" color="#9B9B9B" size={10}/>
                            <Text style={{color: '#9B9B9B', fontSize: 12}}>{item.distance}</Text>
                        </View>

                        <View style={{flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between'}}>
                            <NavegRating value={item.rating} />
                            <Text style={{color: '#378E00', fontFamily: 'Roboto Bold', fontSize: 12}}>{item.averagePrice}</Text>
                        </View>
                    </View>

                  </Card>
                </View>
              </TouchableWithoutFeedback>
              }
            style={{width: '100%'}}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
      );
    } else {
      return(
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', width: '100%' }}>
          <ActivityIndicator size="large" color="#FE740E" />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
    cardContainer: {
      paddingBottom: 10,
      paddingLeft: 20,
      paddingRight: 20,
      paddingTop: 10,
    },
    bestCardContainer: {
      height: `auto`,
      paddingBottom: 10,
      paddingLeft: 5,
      paddingRight: 5,
      paddingTop: 10,
      zIndex: 1
    },
    bestRating: {
      backgroundColor: '#fff',
      right: 1,
      paddingBottom: 1,
      paddingLeft: 3,
      paddingRight: 3,
      paddingTop: 1,
      position: 'absolute',
      top: 7,
      zIndex: 1000,
    },
    listTitle: {
        color: '#979797',
        fontFamily: 'Kayak Sans Bold',
        fontSize: 13,
        marginLeft: 20,
        marginTop: 20
    },
    card: {
        borderWidth: 0,
        borderRadius: 12,
        elevation: 4,
        overflow: 'hidden',
        padding: 0,
        margin: 0
    },
    bestCard: {
        borderWidth: 0,
        borderRadius: 12,
        elevation: 4,
        overflow: 'hidden',
        padding: 0,
        margin: 0,
        maxWidth: 157,
        zIndex: 1,
    },
    bestRestaurantImage: {
      backgroundColor: '#e5e5e5',
      height: 76,
      width: 157
    },
    restaurantImage: {
        backgroundColor: '#e5e5e5',
        height: 85,
        width: 85
    },
    favorite: {
        position: 'absolute',
        right: -10,
        top: -7
    },
    swiper: {
        backgroundColor: '#eee',
    },
    swiperPaginationDot: {
        height: 6,
        marginLeft: 1,
        marginRight: 1,
        width: 6
    },
});
