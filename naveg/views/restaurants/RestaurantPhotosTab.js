import React from 'react';
import { StyleSheet, View, Image, Text, ScrollView } from 'react-native';
import NavegSectionTitle from '../../components/NavegSectionTitle';

export default class RestaurantPhotosTab extends React.Component {
    render() {
        return (
            <ScrollView style={{backgroundColor: '#fff'}}>

                <View style={styles.section}>
                    
                    <NavegSectionTitle title="ÁLBUNS" />

                    <ScrollView horizontal={true} style={{marginLeft: -20, marginRight: -20, width: `auto`}}>

                        <View style={styles.albumItem}>
                            <View style={{width: 128, height: 3, backgroundColor: '#BCBCBC', alignSelf: 'center' }}></View>
                            <View style={{width: 136, height: 3, backgroundColor: '#919191', alignSelf: 'center' }}></View>
                            <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/album01.png"}} style={styles.albumImage} />
                            <View style={styles.albumItemFooter}>
                                <Text style={styles.albumTitle}>Comidas veganas</Text>
                                <Text style={styles.albumSubtitle}>Feitas na hora</Text>
                            </View>
                        </View>

                        <View style={styles.albumItem}>
                            <View style={{width: 128, height: 3, backgroundColor: '#BCBCBC', alignSelf: 'center' }}></View>
                            <View style={{width: 136, height: 3, backgroundColor: '#919191', alignSelf: 'center' }}></View>
                            <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/album02.png"}} style={styles.albumImage} />
                            <View style={styles.albumItemFooter}>
                                <Text style={styles.albumTitle}>Estabelecimento</Text>
                                <Text style={styles.albumSubtitle}>Decoração</Text>
                            </View>
                        </View>

                        <View style={styles.albumItem}>
                            <View style={{width: 128, height: 3, backgroundColor: '#BCBCBC', alignSelf: 'center' }}></View>
                            <View style={{width: 136, height: 3, backgroundColor: '#919191', alignSelf: 'center' }}></View>
                            <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/album03.png"}} style={styles.albumImage} />
                            <View style={styles.albumItemFooter}>
                                <Text style={styles.albumTitle}>Estabelecimento</Text>
                                <Text style={styles.albumSubtitle}>Cardápio</Text>
                            </View>
                        </View>

                        <View style={[styles.albumItem, {marginRight: 20}]}>
                            <View style={{width: 128, height: 3, backgroundColor: '#BCBCBC', alignSelf: 'center' }}></View>
                            <View style={{width: 136, height: 3, backgroundColor: '#919191', alignSelf: 'center' }}></View>
                            <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/album01.png"}} style={styles.albumImage} />
                            <View style={styles.albumItemFooter}>
                                <Text style={styles.albumTitle}>Comidas veganas</Text>
                                <Text style={styles.albumSubtitle}>Feitas na hora</Text>
                            </View>
                        </View>

                    </ScrollView>
                    
                </View>

                <View style={[styles.section, {paddingBottom: 32}]}>
                    
                    <NavegSectionTitle title="FOTOS" />

                    <View style={{flexDirection: 'row', flexWrap: 'wrap', marginRight: -2, marginLeft: -2, width: `auto`}}>
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos01.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos02.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos03.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos04.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos01.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos02.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos03.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos04.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos01.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos02.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos03.jpg"}} style={styles.restaurantImage} />
                        <Image source={{uri: "http://sigmaapp.com.br/naveg/img/restaurants/fotos04.jpg"}} style={styles.restaurantImage} />
                    </View>

                </View>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    section: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 25
    },
    albumItem: {
        marginLeft: 20,
        width: 144
    },
    albumImage: {
        backgroundColor: '#f0f0f0',
        height: 188,
        width: 144
    },
    albumItemFooter: {
        backgroundColor: 'rgba(0,0,0,0.54)',
        bottom: 0,
        height: `auto`,
        padding: 14,
        position: 'absolute',
        width: '100%'
    },
    albumTitle: {
        color: '#fff',
        fontFamily: 'Kayak Sans',
        fontSize: 14,
        fontWeight: '400',
    },
    albumSubtitle: {
        color: 'rgba(255,255,255,0.87)',
        fontFamily: 'Kayak Sans',
        fontSize: 12,
        fontWeight: '400',
    },
    restaurantImage: {
        backgroundColor: '#f0f0f0',
        height: 108,
        margin: 2,
        width: '32%'
    }
});
