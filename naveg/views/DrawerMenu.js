import React from 'react';
import { View, Text, SafeAreaView } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { ThemeContext, getTheme } from 'react-native-material-ui';
import NavegIcon from '../components/NavegIcon';
import NavegDrawer from '../components/NavegDrawer';
import NavegHeader from '../components/NavegHeader';
import * as Views from './index';

const uiTheme = {
    palette: {
        primaryColor: '#FF6D00',
    },
    toolbar: {
        container: {
            height: 60,
        },
        titleText: {
        	fontSize: 20,
            fontFamily: 'Roboto Medium',
            fontWeight: '400'
        }
    },
};

const RootStack = createStackNavigator(
	{
		RestaurantListView: Views.RestaurantListView,
        RestaurantView: {
            screen: Views.RestaurantView,
            navigationOptions: () => ({
                title: "Restaurante",
                header: props => <NavegHeader action="arrow-back" {...props} />,
                tabBarVisible: false
            }),
        }
	},
	{
		initialRouteName: 'RestaurantListView',
		navigationOptions: {
			gesturesEnabled: false,
			headerMode: 'none',
            header: props => <NavegHeader action="menu" {...props} />,
		}
	}
);

RootStack.navigationOptions = ({ navigation }) => {
	let tabBarVisible = true;
	if (navigation.state.index > 0) {
		tabBarVisible = false;
	}

	return {
		tabBarVisible,
	};
};

const RootTabs = createBottomTabNavigator(
	{
		Restaurants: {
			screen: RootStack,
			navigationOptions: {
				title: 'Restaurantes',
				tabBarIcon: ({ tintColor }) => <NavegIcon name="fork_spoon" height={19} width={20} fill={tintColor} />,
			}
		},
		Stores: {
			screen: Views.StoreListView,
			navigationOptions: {
				title: 'Lojas',
				tabBarIcon: ({ tintColor }) => <NavegIcon name="cart" height={20} width={20} fill={tintColor} />,
			}
		},
		Events: {
			screen: Views.EventListView,
			navigationOptions: {
				title: 'Eventos',
				tabBarIcon: ({ tintColor }) => <NavegIcon name="people" height={14} width={22} fill={tintColor} />,
			}
		}
	},
	{
		initialRouteName: 'Restaurants',
		tabBarOptions: {
			activeTintColor: '#FE740E',
			activeBackgroundColor: 'transparent',
			inactiveTintColor: '#979797',
			inactiveBackgroundColor: 'transparent',
			labelStyle: {
				fontFamily: 'Kayak Sans',
				fontSize: 14,
				fontWeight: '400'
			},
			activeLabelStyle: {
				fontFamily: 'Kayak Sans Bold',
				fontWeight: '700'
			},
			tabStyle: {
				borderTopColor: 0,
				borderTopColor: 'transparent',
				padding: 10
			},
			style: {
				borderTopColor: 0,
				borderTopColor: 'transparent',
				elevation: 20,
				height: 64
			}
		}
	}
);

// Criando o DrawerNavigator
const RootDrawer = createDrawerNavigator(
	{
		Home: {
			screen: RootTabs,
			navigationOptions: {
				title: 'Início',
				drawerIcon: ({ tintColor }) => <NavegIcon name="naveg" height={23} width={15} fill={tintColor} />,
			}
		},
		Notifications: {
			screen: Views.Notifications
		},
		Map: {
			screen: Views.Map
		},
		Favorites: {
			screen: Views.Favorites
		},
		Feedback: {
			screen: Views.Feedback
		},
		BeAPartner: {
			screen: Views.BeAPartner
		}
	},
	{
		initialRouteName: 'Home',
		contentComponent: NavegDrawer,
		gesturesEnabled: true,
		contentOptions: {
			activeTintColor: '#FE740E',
			activeBackgroundColor: 'transparent',
			inactiveTintColor: '#737373',
			inactiveBackgroundColor: 'transparent',
			labelStyle: {
				fontFamily: 'Kayak Sans',
				fontSize: 16,
				fontWeight: '400'
			},
			activeLabelStyle: {
				fontFamily: 'Kayak Sans Bold',
			}
		}
	}
);

/* Ainda não entendi bem o porquê, mas tive que adicionar um container ao RootDrawer para
funcionar na versão mais atual do React Native
https://stackoverflow.com/questions/53367195/invariant-violation-the-navigation-prop-is-missing-for-this-navigator */
const App = createAppContainer(RootDrawer);

export default class DrawerMenu extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
			<SafeAreaView style={{ flex: 1, backgroundColor: "#B24C00" }}>
				<ThemeContext.Provider value={getTheme(uiTheme)}>
					<App />
				</ThemeContext.Provider>
			</SafeAreaView>
		);
    }
}