import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';

export class Notifications extends React.Component {
    static navigationOptions = {
        title: 'Avisos',
        drawerIcon: ({ tintColor }) => <Icon name="notifications" color={tintColor} />,
    };

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <View>
                <Text>
                    Avisos
                </Text>
            </View>
        );
    }
}