import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';
import * as Views from './index';

export class Map extends React.Component {
    static navigationOptions = {
        title: 'Mapa',
        drawerIcon: ({ tintColor }) => <Icon name="map" color={tintColor} />,
    };

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <View>
                <Text>
                    Mapa
                </Text>
            </View>
        );
    }
}