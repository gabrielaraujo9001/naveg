import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from 'react-native-elements';

export class Favorites extends React.Component {
    static navigationOptions = {
        title: 'Favoritos',
        drawerIcon: ({ tintColor }) => <Icon name="favorite" color={tintColor} />,
    };

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <View>
                <Text>
                    Favoritos
                </Text>
            </View>
        );
    }
}