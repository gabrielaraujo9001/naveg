import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import NavegIcon from '../components/NavegIcon';

export class BeAPartner extends React.Component {
    static navigationOptions = {
        title: 'Seja um parceiro',
        drawerIcon: ({ tintColor }) => <NavegIcon name="flag" height={18} width={16} fill={tintColor} />,
    };

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <View>
                <Text>
                    Seja um parceiro
                </Text>
            </View>
        );
    }
}