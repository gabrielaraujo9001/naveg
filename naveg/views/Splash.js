import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

export default class Splash extends React.Component {
  render() {
    return (
      <View style={styles.background}>
        <Image style={{width: 114, height: 60, marginBottom: 30}} source={require('./../img/logo.png')} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  }
});