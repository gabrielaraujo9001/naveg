import React from 'react';
import { Text, View, Image } from 'react-native';
import PropTypes from 'prop-types';
import NavegIcon from '../NavegIcon';
import styles from './styles';

const NavegRating = (props) => {
    const { value, expanded } = props;

    formatValue = (v) => {
        let decimalValue = v.toFixed(1);
        return decimalValue.replace(/\./g,",");
    }

    prepareRating = (v, e) => {
    	let color = "#FDB709";
    	let rating = [];

    	if(e) {
    		for(i=0; i<5; i++) {
    			if(i>=v)
    				color = "#b1b1b1";
    			rating.push(<NavegIcon key={i} name="star" height={11} width={11} fill={color} style={styles.star} />);
    		}
    	} else {
    		rating.push(<Text key="number" style={styles.number}>{ formatValue(v) }</Text>);
    		rating.push(<NavegIcon key="star" name="star" height={11} width={11} fill={color} style={styles.star} />);
    	}

    	return (rating);
    }

    return (
	    <View style={styles.ratingContainer} >
    		{ prepareRating(value, expanded) }
    	</View>
    );
};

NavegRating.propTypes = {
	value: PropTypes.number,
	expanded: PropTypes.bool
};

export default NavegRating;