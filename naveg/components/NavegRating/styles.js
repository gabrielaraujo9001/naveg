import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    ratingContainer: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    number: {
        color: '#979797',
        fontFamily: 'Roboto',
        fontSize: 12,
        marginRight: 2
    },
    star: {
        marginLeft: 3
    }
});

export default styles;