import React from 'react';
import { TouchableOpacity, View, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import SvgIcon from 'react-native-svg-icon';
import svgs from '../../assets/icons';

const NavegIcon = (props) => {
  const {
    containerStyle, onPress, color, ...icon
  } = props;

  return (
    <View
      onPress={onPress}
      style={containerStyle}
    >
      <SvgIcon
        width={14}
        height={14}
        fill={color}
        {...icon}
        svgs={svgs}
      />
    </View>
  );
};

NavegIcon.propTypes = {
  containerStyle: ViewPropTypes.style,
  color: PropTypes.string,
  onPress: PropTypes.func,
};

export default NavegIcon;
