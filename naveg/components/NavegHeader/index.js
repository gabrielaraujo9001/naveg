import React from 'react';
import { Toolbar } from 'react-native-material-ui';
import { NavigationActions } from 'react-navigation';
import styles from './styles';

export default class NavegHeader extends React.Component {

    constructor(props) {
        super(props);
        this.state = {name: 'NaVeg'};
    }

    navigate() {
        if(this.props.action === "menu") {
            this.props.navigation.openDrawer();
        } else if (this.props.action === "arrow-back") {
            this.props.navigation.dispatch(NavigationActions.back());
        }
    }

    render() {
        let options = this.props.scene.descriptor.options;
        let action = this.props.action;

        return (
            <Toolbar
                leftElement={action}
                centerElement={options.title}
                searchable={{
                    autoFocus: true,
                    placeholder: 'Buscar',
                }}
                onLeftElementPress={() => this.navigate()}
            />
        );
    }
}