import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	tagWrapper: {
		flexDirection: 'row'
	},
	tag: {
		marginLeft: 2,
		marginRight: 2
	}
});

export default styles;