import React from 'react';
import { Text, View } from 'react-native';
import NavegIcon from '../NavegIcon';
import styles from './styles';

const tagColors = {
    "milkfree": "#1AA8F8",
    "glutenfree": "#EF8609",
    "sugarfree": "#FB3D31"
};

const NavegDishTags = (props) => {
    const { tags } = props;

    prepareTags = (t) => {
        let tagsArray = [];

        t.map((tag, i) => {
            tagsArray.push(
                <NavegIcon key={i} name={tag} height={16} width={16} fill={tagColors[tag]} style={styles.tag} />
            );
        });

        return tagsArray;
    }

    return (
        <View style={styles.tagWrapper}>
        	{ prepareTags(tags) }
        </View>
    );
};

export default NavegDishTags;