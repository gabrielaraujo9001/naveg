import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    section: {
        borderBottomColor: '#DEDEDE',
        borderBottomWidth: 1,
        padding: 20
    },
    sectionContent: {
        color: '#878787',
        fontSize: 14
    },
    days: {
    	flexDirection: 'row',
    	justifyContent: 'space-between',
    	width: '75%'
    }
});

export default styles;
