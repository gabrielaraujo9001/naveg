import React from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native';
import NavegIcon from '../../components/NavegIcon';
import NavegSectionTitle from '../../components/NavegSectionTitle';
import NavegTimeTable from '../../components/NavegTimeTable';
import call from 'react-native-phone-call';
import styles from './styles';

const RestaurantAbout = (props) => {
    const { restaurant } = props;
    let args = {};

    renderPhoneNumbers = (phoneNumbers) => {
        let numbers = "";

        phoneNumbers.map((phone, i) => {
            numbers += phone;
            if(!(phoneNumbers.length === i+1)) {
                numbers += "  |  ";
            }
        });

        args = {
            number: phoneNumbers[0],
            prompt: false
        }

        return numbers;
    }

    callNumber = () => {
        call(args).catch(console.error);
    }

    return (
        <View>
            <View style={styles.section}>
                <NavegSectionTitle title="ENDEREÇO" />
                <Text style={styles.sectionContent}>
                    {restaurant.street}{"\n"}{restaurant.city}
                </Text>
            </View>
            <View style={styles.section}>
                <NavegSectionTitle title="TELEFONE" />
                <TouchableWithoutFeedback onPress={callNumber}>
                    <View>
                        <Text style={styles.sectionContent}>
                            { renderPhoneNumbers(restaurant.phone) }
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>

            <NavegTimeTable timetable={restaurant.timetable} />

        </View>
    );
}

export default RestaurantAbout;
