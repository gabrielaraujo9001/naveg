import React from 'react';
import { View } from 'react-native';
import NavegUserComment from '../NavegUserComment';
import NavegSectionTitle from '../NavegSectionTitle';

const NavegUserCommentList = (props) => {
    const { commentList } = props;

    prepareComments = (commentList) => {
        let commentsArray = [];

        commentList.map((comment, i) => {
            commentsArray.push(
                <NavegUserComment key={i} comment={comment}></NavegUserComment>
            );
        });

        return commentsArray;
    }

    return (
        <View style={{padding: 20}}>
            <NavegSectionTitle title="AVALIAÇÕES" />
            { prepareComments(commentList) }
        </View>
    );
};

export default NavegUserCommentList;
