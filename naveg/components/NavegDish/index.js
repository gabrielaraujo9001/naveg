import React from 'react';
import { Text, View, Image } from 'react-native';
import NavegIcon from '../NavegIcon';
import NavegRating from '../NavegRating';
import NavegPrice from '../NavegPrice';
import NavegDishTags from '../NavegDishTags';
import NavegFavoriteButton from '../NavegFavoriteButton';
import styles from './styles';

const NavegDish = (props) => {
    const { dish } = props;

    return (
        <View style={styles.dish}>
            <Image source={{uri: dish.image}} style={styles.image} />
            <View style={styles.infoWrapper}>
                <View>
                    <Text style={styles.name}>{dish.name}</Text>
                    <Text style={styles.description}>{dish.description}</Text>
                    <View style={styles.favorite}>
                        <NavegFavoriteButton />
                    </View>
                </View>
                <View style={styles.bottomInfo}>
                    <NavegRating value={dish.rating} />
                    <NavegDishTags tags={dish.tags} />
                    <NavegPrice price={dish.price} />
                </View>
            </View>
        </View>
    );
};

export default NavegDish;