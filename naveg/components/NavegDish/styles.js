import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    dish: {
    	borderBottomColor: '#DEDEDE',
    	borderBottomWidth: 1,
    	flexDirection: 'row',
    	paddingBottom: 15,
    	paddingLeft: 20,
    	paddingRight: 20,
    	paddingTop: 15
    },
    image: {
    	borderRadius: 10,
    	height: 72,
    	marginRight: 13,
    	width: 72
    },
    infoWrapper: {
    	flex: 1,
        position: 'relative'
    },
    name: {
    	color: '#4F4F4F',
    	fontFamily: 'Kayak Sans Bold',
    	fontSize: 16
    },
    favorite: {
        position: 'absolute',
        right: -10,
        top: -10
    },
    description: {
    	color: '#979797',
    	fontFamily: 'Roboto Light',
    	fontSize: 14,
        marginBottom: 5
    },
    bottomInfo: {
        alignItems: 'center',
    	flexDirection: 'row',
    	justifyContent: 'space-between',
    	width: '100%'
    }
});

export default styles;