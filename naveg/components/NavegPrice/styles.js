import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    price: {
    	color: '#378E00',
    	fontFamily: 'Roboto Bold',
    	fontSize: 12
    }
});

export default styles;