import React from 'react';
import { Text } from 'react-native';
import styles from './styles';

const NavegPrice = (props) => {
    const { price } = props;

    formatPrice = (p) => {
    	let decimalPrice = p.toFixed(2);
    	return "R$ "+decimalPrice.replace(/\./g,",");
    }

    return (
        <Text style={styles.price}>{ formatPrice(price) }</Text>
    );
};

export default NavegPrice;