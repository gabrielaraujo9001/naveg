import React from 'react';
import { Text, View, Image } from 'react-native';
import NavegIcon from '../NavegIcon';
import NavegRating from '../NavegRating';
import styles from './styles';

const NavegUserComment = (props) => {
    const { comment } = props;

    return (
        <View style={styles.userCommentContainer}>
            <Image source={{uri: comment.avatar}} style={styles.userAvatar} />
            <View style={styles.commentWrapper}>
                <Text style={styles.userName}>{comment.name}</Text>
                <NavegRating value={comment.rating} expanded />
                <Text style={styles.comment}>{comment.comment}</Text>
            </View>
        </View>
    );
};

export default NavegUserComment;
