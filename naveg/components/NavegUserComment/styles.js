import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    userCommentContainer: {
        alignItems: 'flex-start',
        flexDirection: 'row',
        paddingBottom: 7,
        paddingTop: 7,
        width: '100%'
    },
    userAvatar: {
        borderRadius: 46,
        height: 46,
        marginRight: 10,
        width: 46
    },
    commentWrapper: {
        borderBottomWidth: 1,
        borderBottomColor: '#DEDEDE',
        flex: 1,
        paddingBottom: 15,
        paddingLeft: 5,
        paddingRight: 0,
        paddingTop: 8,
        position: 'relative'
    },
    userName: {
        color: '#565656',
        fontFamily: 'Kayak Sans Bold',
        fontSize: 14,
        marginBottom: 2
    },
    comment: {
        color: '#878787',
        fontFamily: 'Roboto',
        fontSize: 12,
        marginTop: 5
    }
});

export default styles;