import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	buttonContainer: {
		alignItems: 'center',
		justifyContent: 'center',
		padding: 10
	}
});

export default styles;