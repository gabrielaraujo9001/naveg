import React from 'react';
import { Button, View, TouchableWithoutFeedback, TouchableHighlight, Alert } from 'react-native';
import NavegIcon from '../NavegIcon';
import styles from './styles';

export default class NavegFavoriteButton extends React.Component {

    color = "#979797";

    constructor(props) {
        super(props);

        this.state = {active: false};
    }

    _onPressButton = () => {
        this.setState({active: !this.state.active});
    }

    render() {
        if(this.state.active) {
            this.color = "#FF6D00";
        } else {
            this.color = "#979797";
        }

        return (
            <TouchableWithoutFeedback onPress={this._onPressButton} >
                <View style={styles.buttonContainer}>
                    <NavegIcon name="heartfill" height={12} width={13} fill={this.color} />
                </View>
            </TouchableWithoutFeedback>
        );
    }
}