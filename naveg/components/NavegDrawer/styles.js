import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    drawerContainer: {
        backgroundColor: '#FAFAFA'
    },
    header: {
        backgroundColor: '#FE740E',
        flexDirection: 'row',
        paddingBottom: 20,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 20,
    },
    headerAvatar: {
        borderRadius: 120,
        height: 70,
        marginRight: 10,
        width: 70
    },
    headerGreeting: {
        color: '#fff',
        fontFamily: 'Roboto Medium',
        fontSize: 18,
        marginBottom: 0
    },
    lineSeparator: {
        borderBottomWidth: 1,
        borderBottomColor: '#f69044',
        marginBottom: 8,
        width: '100%'
    },
    level: {
        color: '#fff',
        fontFamily: 'Roboto Light',
        fontSize: 12,
        marginBottom: 10
    },
    email: {
        color: '#fff',
        fontFamily: 'Kayak Sans',
        fontSize: 12
    },
    drawerItemsContainer: {
        borderBottomColor: '#e7e7e7',
        borderBottomWidth: 1,
        paddingBottom: 5
    },
    footer: {

    },
    footerItensContainer: {
        paddingTop: 8
    },
    footerItem: {
        color: '#737373',
        fontFamily: 'Kayak Sans',
        fontSize: 16,
        fontWeight: '400',
        padding: 16
    }
});

export default styles;