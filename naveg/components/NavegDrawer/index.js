import React from 'react';
import { ScrollView, View, Text, Image } from 'react-native';
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import styles from './styles';

const NavegDrawer = props => (
    <ScrollView style={styles.drawerContainer}>
        <View style={styles.header}>
            <Image source={{uri: "http://sigmaapp.com.br/naveg/img/users/jonathan.jpg"}} style={styles.headerAvatar}/>
            <View>
                <Text style={styles.headerGreeting}>Bom dia, Jonathan!</Text>
                <Text style={styles.level}>Vegano</Text>
                <View style={styles.lineSeparator}></View>
                <Text style={styles.email}>jonathan@gmail.com</Text>
            </View>
        </View>
        <View style={styles.drawerItemsContainer} forceInset={{ top: 'always', horizontal: 'never' }}>
            <DrawerNavigatorItems {...props} />
        </View>
        <View style={styles.footer}>
            <View style={styles.footerItensContainer}>
                <Text style={styles.footerItem}>Termos de uso</Text>
                <Text style={styles.footerItem}>Sobre</Text>
            </View>
        </View>
    </ScrollView>
);

export default NavegDrawer;