import React from 'react';
import { Text, View } from 'react-native';
import { Icon } from 'react-native-elements';
import NavegSectionTitle from '../../components/NavegSectionTitle';
import styles from './styles';

export default class NavegTimeTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {active: false};
    }

    renderTimeTable = (times) => {
        let days = [ "Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado" ];
        let timeTable = [];

        if(this.state.active) {
            times.map((time, i) => {
                timeTable.push(
                    <View key={days[i]} style={styles.days}>
                        <Text style={styles.sectionContent}>{days[i]}</Text>
                        <Text style={styles.sectionContent}>{time.open + " às " + time.close}</Text>
                    </View>
                );
            });
        } else {
            timeTable.push(
                <View key={days[0]} style={styles.days}>
                    <Text style={styles.sectionContent}>{days[0]}</Text>
                    <Text style={styles.sectionContent}>{times[0].open + " às " + times[0].close}</Text>
                </View>
            );
        }

        return (timeTable);
    }

    toggleTimeTable = () => {
        this.setState({active: !this.state.active});
    }

    render() {

        let icon = null;

        if(this.state.active) {
            icon = "arrow-drop-up";
        } else {
            icon = "arrow-drop-down";
        }

        return (
            <View style={styles.section}>
                <NavegSectionTitle title="HORÁRIO" />
                <View style={{position: 'relative'}}>
                    <View>
                        { this.renderTimeTable(this.props.timetable) }
                    </View>
                    <View style={{position: 'absolute', bottom: 0, right: 20}}>
                        <Icon name={icon} color="#979797" size={20} onPress={this.toggleTimeTable} />
                    </View>
                </View>
            </View>
        );
    }
}
