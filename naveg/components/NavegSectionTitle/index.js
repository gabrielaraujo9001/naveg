import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

const NavegSectionTitle = (props) => {
    const { title } = props;

    return (
        <View>
            <Text style={styles.sectionTitle}>{title}</Text>
            <View style={styles.sectionTitleLine}></View>
        </View>
    );
};

export default NavegSectionTitle;
