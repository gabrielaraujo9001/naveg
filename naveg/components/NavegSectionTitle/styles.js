import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    sectionTitle: {
        color: '#646464',
        fontFamily: 'Kayak Sans Bold',
        fontSize: 14,
        marginBottom: 4
    },
    sectionTitleLine: {
        borderBottomWidth: 1,
        borderBottomColor: '#FE740E',
        marginBottom: 10,
        width: 24
    }
});

export default styles;