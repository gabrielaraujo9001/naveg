import React from 'react';
import { View } from 'react-native';
import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyC-dcnpPVIp7VgbNxHFTEb5H8euOmaFCBw",
    authDomain: "naveg-5e622.firebaseapp.com",
    databaseURL: "https://naveg-5e622.firebaseio.com",
    storageBucket: "naveg-5e622.appspot.com"
}

const Firebase = firebase.initializeApp(firebaseConfig);

export default Firebase;