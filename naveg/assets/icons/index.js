import React from 'react';
import { G, Path, Rect } from 'react-native-svg';

export default {
    'glutenfree': {
        svg: (
            <G>
                <Path d="M253.1,97.7c-38.9,27.4-38.9,91.8,0,123.1C292,197.4,288.1,115.3,253.1,97.7z"/>
                <Path d="M360.2,222.8c0-11.7,0-19.5-13.6-17.6c-37,3.9-74,37.1-79.8,72.3c-1.9,9.8,0,15.6,11.7,15.6
                    C315.4,295.1,358.2,256,360.2,222.8z"/>
                <Path d="M270.6,390.8c50.6,0,87.6-35.2,89.5-84C307.6,299,258.9,347.8,270.6,390.8z"/>
                <Path d="M146,209.1c3.9,52.8,46.7,91.8,93.4,82.1C241.4,242.3,183,193.5,146,209.1z"/>
                <Path d="M175.2,306.8c-7.8-2-19.5-3.9-25.3,2c-3.9,3.9-1.9,15.6,0,23.4c13.6,35.2,46.7,58.6,89.5,58.6
                    C237.5,349.8,210.3,314.6,175.2,306.8z"/>
                <Path d="M512,254C510,113.3,395.2,0,255,0C111,0-1.9,115.3,0,259.9C2,398.6,116.8,512,257,512C397.1,513.9,513.9,396.7,512,254z
                     M253.1,469c-57.5,0-109.7-23.7-148-61.4l42.9-30.4c-13.6-17.6-17.5-37.1-19.5-52.8l-57.3,40.2C52,332.6,40.9,295.3,40.9,256
                    c0-119.2,97.3-215,214.1-215c68.7,0,129.5,32.1,168.6,82.2L364,164.1c5.8,15.6,9.7,41,13.6,56.7l73.9-50.5
                    c11.3,26.2,17.6,55.2,17.6,85.7C469.1,375.2,373.8,470.9,253.1,469z"/>
            </G>
        ),
        viewBox: "0 0 512 512"
    },

    'milkfree': {
        svg: (
            <G>
                <Path d="M254.1,0C112.5,0-1.9,115.3,0,259.9C2,398.6,116.4,512,256,512c139.6,2,256-115.3,256-257.9C510.1,113.3,395.6,0,254.1,0z
                     M252.1,470.9c-58.6,0-111.8-24.8-150.1-64.3l51.2-35.3v-58.6L72,367.7C52.2,335,40.7,296.7,40.7,256c0-119.2,97-215,215.3-215
                    c69.3,0,130.6,33.5,169.5,84.9l-74.4,51.9l3.9,56.7l95.7-66.3c12,26.9,18.7,56.6,18.7,87.8C469.3,377.1,372.4,470.9,252.1,470.9z"
                    />
                <Path d="M314.2,172c-7.8-5.9-13.6-13.7-21.3-21.5c-7.8-7.8-5.8-23.4,1.9-31.3c3.9-3.9,7.8-9.8,5.8-13.7c0-2-5.8-7.8-9.7-7.8
                    c-23.3,0-46.5,0-69.8,0c-3.9,0-9.7,5.9-11.6,9.8s1.9,9.8,5.8,13.7c7.8,7.8,9.7,23.4,1.9,31.3c-9.7,9.8-21.3,19.5-29.1,29.3
                    c-5.8,7.8-9.7,15.6-9.7,25.4c0,7.8,0,13.7,0,21.5h157.1c0-2,0-2,0-3.9C335.5,203.2,333.6,185.6,314.2,172z"/>
                <Path d="M176.5,314.6c0,23.4,0,46.9,0,68.4c0,19.5,7.8,25.4,27.2,25.4c34.9,0,67.9,0,102.8,0c21.3,0,29.1-7.8,29.1-29.3
                    c0-21.5,0-44.9,0-66.4h-159V314.6z"/>
                <Path d="M333.6,238.4H176.5c0,17.6,0,37.1,0,54.7c0,3.9,0,5.9,0,9.8h157.1C333.6,282,333.6,261.1,333.6,238.4z"/>
            </G>
        ),
        viewBox: "0 0 512 512"
    },

    'sugarfree': {
        svg: (
            <G>
                <Path d="M512,254.1C512,112.9,395.7,0,255.1,0C112.5,0-1.9,116.7,0,259.8C1.9,399,116.3,512,256.9,512
                    C395.7,513.9,513.9,397.2,512,254.1z M253.2,470.6c-59.3,0-113.3-25.5-151.9-66l28.1-20.6v-62.1L70.7,364
                    C52,332.1,41.3,295.1,41.3,256c0-118.6,97.5-214.6,215.7-214.6c64.1,0,121.6,28.3,160.8,73.4L317,188.2v28.2h9.4h37.5l83.1-58.2
                    c15.2,29.8,23.7,63.6,23.7,99.7C470.7,376.5,373.2,470.6,253.2,470.6z"/>
                <Path d="M360.1,235.3c-20.6,0-41.3,0-63.8,0c0-22.6,0-45.2,0-65.9c0-7.5-1.9-9.4-7.5-9.4c-22.5,0-45,0-69.4,0
                    c-3.8,0-5.6,1.9-7.5,3.8c-7.5,7.5-15,16.9-24.4,24.5c-3.8,3.8-5.6,7.5-5.6,13.2c0,20.7,0,41.4,0,62.1c0,3.8-1.9,7.5-3.8,9.4
                    c-7.5,7.5-15,15.1-20.6,22.6c-5.6,1.9-7.5,5.6-7.5,9.4c0,22.6,0,45.2,0,67.8c0,7.5,1.9,9.4,9.4,9.4c22.5,0,45,0,67.5,0
                    c3.8,0,7.5-1.9,9.4-3.8c7.5-7.5,15-13.2,20.6-20.7c3.8-3.8,7.5-5.6,11.3-5.6c20.6,0,41.3,0,61.9,0c5.6,0,9.4-1.9,13.1-5.6
                    c7.5-7.5,16.9-15.1,24.4-24.5c1.9-1.9,3.8-7.5,3.8-9.4c0-22.6,0-43.3,0-65.9C371.3,235.3,371.3,235.3,360.1,235.3z M225.1,370.8
                    c-20.6,0-43.1,0-63.8,0c0-20.7,0-43.3,0-64c20.6,0,43.1,0,63.8,0C225.1,327.5,225.1,348.2,225.1,370.8z M255.1,265.4
                    c-20.6,0-43.1,0-63.8,0c0-20.7,0-43.3,0-64c20.6,0,43.1,0,63.8,0C255.1,222.1,255.1,242.8,255.1,265.4z M330.1,340.7
                    c-22.5,0-43.1,0-63.8,0c0-20.7,0-43.3,0-64c20.6,0,43.1,0,63.8,0C330.1,297.4,330.1,318.1,330.1,340.7z"/>
                <Path d="M294.5,327.5c0,3.8,3.8,5.6,5.6,9.4c0,0,1.9,0,3.8,0c1.9-3.8,5.6-5.6,5.6-9.4s-3.8-7.5-7.5-7.5
                    C296.3,320,294.5,323.8,294.5,327.5z"/>
                <Path d="M279.5,336.9c1.9-3.8,5.6-5.6,5.6-9.4s-3.8-7.5-7.5-7.5c-5.6,0-7.5,3.8-7.5,7.5s3.8,5.6,5.6,9.4
                    C277.6,336.9,277.6,336.9,279.5,336.9z"/>
                <Path d="M292.6,321.9c1.9-3.8,5.6-5.6,5.6-9.4c0-5.6-3.8-7.5-7.5-7.5c-5.6,0-7.5,3.8-7.5,7.5c0,1.9,3.8,5.6,5.6,7.5
                    C290.7,320,290.7,320,292.6,321.9z"/>
                <Path d="M185.7,359.5c0,5.6,3.8,7.5,7.5,7.5c5.6,0,7.5-3.8,7.5-7.5c0-1.9-3.8-5.6-5.6-7.5c0,0-1.9,0-3.8-1.9
                    C189.4,353.9,185.7,355.7,185.7,359.5z"/>
                <Path d="M183.8,350.1c5.6,0,7.5-3.8,7.5-7.5c-1.9-3.8-3.8-5.6-5.6-7.5c-5.6,0-9.4,3.8-9.4,7.5C176.3,348.2,178.2,350.1,183.8,350.1
                    z"/>
                <Path d="M170.7,368.9c3.8,0,7.5-1.9,7.5-7.5s-3.8-7.5-7.5-7.5c-1.9,0-5.6,3.8-7.5,5.6c0,0,0,1.9,0,3.8
                    C165.1,365.2,166.9,368.9,170.7,368.9z"/>
                <Path d="M213.8,244.7c3.8,0,5.6-3.8,9.4-5.6c0-1.9,0-1.9-1.9-3.8s-3.8-3.8-7.5-5.6c-3.8-1.9-7.5,3.8-7.5,7.5
                    C206.3,242.8,210.1,244.7,213.8,244.7z"/>
                <Path d="M204.4,265.4c1.9-3.8,5.6-5.6,5.6-9.4c0-3.8-3.8-7.5-7.5-7.5c-5.6,0-7.5,3.8-7.5,7.5c0,1.9,3.8,5.6,5.6,7.5
                    C202.6,263.5,202.6,263.5,204.4,265.4z"/>
                <Path d="M217.6,256c0,3.8,1.9,7.5,7.5,7.5c5.6,0,7.5-3.8,7.5-7.5c0-3.8-3.8-5.6-5.6-9.4c0,0-1.9,0-3.8,1.9
                    C221.3,250.3,217.6,252.2,217.6,256z"/>
            </G>
        ),
        viewBox: "0 0 512 512"
    },

    'pin': {
        svg: (
            <Path d="M182.9,0C81.8,0,0,80.1,0,179.2C0,313.6,182.9,512,182.9,512s182.9-198.4,182.9-332.8C365.7,80.1,284,0,182.9,0z
             M182.9,243.2c-36,0-65.3-28.7-65.3-64s29.3-64,65.3-64s65.3,28.7,65.3,64S218.9,243.2,182.9,243.2z"/>
        ),
        viewBox: "0 0 365.7 512"
    },

    'star': {
        svg: (
            <Path d="M256,411.5L414.2,512l-42-189.4L512,195.1l-184.1-16.4L256,0l-71.9,178.7L0,195.1l139.8,127.5L97.8,512L256,411.5z"/>
        ),
        viewBox: "0 0 512 512"
    },

    heart: {
        svg: (
            <Path d="M256,496l-21.9-23.1c-26.8-28.3-52-53.8-75.3-77.3C66.6,302.4,0,235.1,0,148.8C0,65.4,62.8,0,143.1,0
            c43.2,0,84.6,19.9,112.9,51.9C284.3,19.9,325.7,0,368.9,0C449.1,0,512,65.4,512,148.8c0,86.3-66.6,153.6-158.8,246.7
            c-23.3,23.5-48.6,49-75.3,77.4L256,496z M143.1,60.3c-47.2,0-82.8,38.1-82.8,88.5c0,61.5,56.2,118.3,141.4,204.3
            c17.1,17.3,35.3,35.6,54.4,55.4c19.1-19.8,37.3-38.1,54.4-55.5c85.1-86,141.4-142.8,141.4-204.2c0-50.5-35.6-88.5-82.8-88.5
            c-36.4,0-72,25-84.7,59.4L256,196.5l-28.3-76.8C215.3,85.8,178.9,60.3,143.1,60.3z"/>
        ),
        viewBox: "0 0 512 496"
    },

    heartfill: {
        svg: (
            <Path d="M256,88.5C236.9,37.4,183.6,0,128,0C55.7,0,0,57,0,132.8C0,236.9,107.9,317.4,256,472c148.1-154.6,256-235.1,256-339.3
            C512,57,456.3,0,384,0C328.4,0,275.1,37.4,256,88.5z"/>
        ),
        viewBox: "0 0 512 472"
    },

    filter: {
        svg: (
            <G>
                <Path d="M80.9,133.8V0H21.6v137.2C8.5,146.9,0,162.2,0,179.5c0,17.3,8.5,32.6,21.6,42.2V512h59.3V225.2c16.1-9.1,27-26.2,27-45.7
                    C107.8,159.9,97,142.9,80.9,133.8z"/>
                <Path d="M380,179.5c0-19.5-10.8-36.6-27-45.7V0h-59.3v137.2c-13.1,9.6-21.6,25-21.6,42.2c0,17.3,8.5,32.6,21.6,42.2V512H353V225.2
                    C369.2,216,380,199,380,179.5z"/>
                <Path d="M218.3,254.1V0H159v254.1c-14.6,9.4-24.3,25.7-24.3,44.1c0,18.4,9.6,34.6,24.3,44.1V512h59.3V342.3
                    c14.6-9.4,24.3-25.7,24.3-44.1C242.6,279.8,232.9,263.6,218.3,254.1z"/>
            </G>
        ),
        viewBox: "0 0 380 512"
    },

    naveg: {
        svg: (
            <G>
                <Path d="M335,226.9c-1.8-15.4-6.2-30-12.3-43.5c-0.2,14.9-1.4,29.3-4.3,42.8c-2.9,13.6-7.4,26.3-13.5,38.1
                    c-6,11.9-13.6,22.9-21.7,34.3L270.6,316l-6.5,9l-3.3,4.6l-3,3.9c-7.6,10.4-16.9,21-25.9,31.8l-28.1,32l-34.4,38.6l-7.9-9l-31-34.8
                    l-15.8-17.2l-7.9-8.6l-7.2-8.3c-4.8-5.5-9.9-10.8-14.4-16.6c-4.4-5.8-9-11.4-13.5-17.2c-17-23.4-31.8-48.5-41-76
                    c-8.9-27.4-13.6-57.6-6.3-88.6C14.3,172.2,7.8,187.8,4,204c-3.7,16.2-4.8,33.2-3.5,50.1c2.4,33.9,14.2,66.4,29.7,96
                    c4.2,7.2,8.3,14.5,12.6,21.6c4.4,7.1,9.4,13.6,14.1,20.5c2.4,3.4,4.8,6.8,7.2,10.1l7.2,9.2l14.5,18.5c9.9,12.1,19.6,24.4,29.7,36.4
                    l15.3,18l7.9,9.1l4.3,4.6l2.4,2.5l1.6,1.6l1.4,1.3l0.4,0.4l1.2,1c0.6,0.5,1.2,1,1.8,1.4c1.2,0.7-1.3-0.7,2.8,1.8
                    c6.9,4.3,18.6,5,25.5,2l11.8-5.1l3.3-6.5c18.5-21.2,36.1-42.7,53-65.1c8.9-11.7,17.6-23.7,26-36.1c8.5-12.4,16.4-25.1,24-39.4
                    l2.7-5.3l2.2-4.8l4.6-9.7c3.2-6.5,6.4-13.2,9.5-20.1c6.3-13.8,11.9-28.7,15.3-44.3C336,258.4,336.9,242.3,335,226.9z"/>
                <Path d="M148.4,272.7c6.4,8.2,13,22.3,21.1,29c1.8,1.5,7.3,0.5,5.4-1.7c-18.6-14.1-27.5-49.2-32.9-71.8c-6.2-26,1.3-49.8,15.9-71.6
                    c3.9-5.8,8-11.4,12.3-17.1c0.1-0.1,0.2-0.2,0.3-0.3c1.1-0.8,2.7,0.5,2,1.8c-4.3,7.7-8.5,15.3-12.3,23c-18.4,37.6-4.6,83.4,6.6,75.2
                    c0.9-0.7,1.8-1.7,2.6-3.1c12-19.9,32.5-35.1,48.8-52.6c5.8-6.1,11-12.6,15.3-19.6c16.8-28,19.5-60.5,12.2-91.8
                    c-5.4-23.4-17.9-46.5-25.7-69.2c-1-2.9-4.1-3.7-6.2-2.2c-0.5,0.4-1,0.9-1.3,1.6c-12.6,27-35.7,46.2-57.4,66.3
                    c-10.6,9.9-20.8,19.9-29.2,31.3c-1.8,2.5-3.6,5-5.3,7.6c-4.9,7.7-9,16-11.9,25.2c-2.7,8.6-3.7,17.3-3.6,25.9
                    c0.2,13.7,3.3,27.4,7.6,40.9C120.4,224,132.6,252.6,148.4,272.7z"/>
            </G>
        ),
        viewBox: "0 0 336 512"
    },

    cart: {
        svg: (
            <Path d="M153.6,409.6c-28.3,0-50.9,22.9-50.9,51.2c0,28.3,22.7,51.2,50.9,51.2c28.3,0,51.2-22.9,51.2-51.2
                C204.8,432.5,181.9,409.6,153.6,409.6z M409.6,409.6c-28.3,0-50.9,22.9-50.9,51.2c0,28.3,22.7,51.2,50.9,51.2
                c28.3,0,51.2-22.9,51.2-51.2C460.8,432.5,437.9,409.6,409.6,409.6z M51.2,0H0v51.2h51.2l92.1,194.3l-34.6,62.7
                c-4,7.3-6.3,15.7-6.3,24.6c0,28.3,22.9,51.2,51.2,51.2h307.2v-51.2H164.4c-3.5,0-6.4-2.8-6.4-6.4l0.8-3.1l23-41.7h190.7
                c19.2,0,36-10.6,44.8-26.3l91.5-166.1c2-3.6,3.2-7.9,3.2-12.3c0-14.2-11.5-25.6-25.6-25.6H107.9L97.5,29.3C89.3,12,71.6,0,51.2,0z"
                />
        ),
        viewBox: "0 0 512 512"
    },

    flag: {
        svg: (
            <G>
                <Rect class="st0" width="64" height="512"/>
                <Rect x="96" y="0" class="st0" width="352" height="352"/>
            </G>
        ),
        viewBox: "0 0 448 512"
    },

    fork_spoon: {
        svg: (
            <Path d="M143.5,273.2l75.8-75.8L31.3,9.8c-41.8,41.8-41.8,109.5,0,151.5L143.5,273.2z M325.1,224.8c41,19,98.5,5.6,141.1-36.9
                c51.1-51.1,61.1-124.5,21.7-163.8C448.8-15.1,375.4-5.4,324,45.7c-42.6,42.6-56,100.1-37,141.1L25.7,448l37.8,37.7L248,301.8
                L432.2,486l37.8-37.7L285.7,264.1L325.1,224.8z"/>
        ),
        viewBox: "0 0 512 486"
    },

    people: {
        svg: (
            <Path d="M162.9,186.3C108.6,186.3,0,213.5,0,267.8V326h325.8v-58.2C325.8,213.5,217.3,186.3,162.9,186.3z M349.1,186.3
                c-6.8,0-14.4,0.4-22.5,1.3c26.9,19.5,45.7,45.7,45.7,80.2V326H512v-58.2C512,213.5,403.4,186.3,349.1,186.3z M162.9,0
                c-38.5,0-69.8,31.3-69.8,69.9s31.3,69.9,69.8,69.9c38.6,0,69.6-31.3,69.6-69.9S201.5,0,162.9,0z M349.1,0
                c-38.6,0-69.8,31.3-69.8,69.9s31.3,69.9,69.8,69.9c38.5,0,69.6-31.3,69.6-69.9S387.7,0,349.1,0z"/>
        ),
        viewBox: "0 0 512 326"
    },

};